import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic-multi-page-form-demo',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
