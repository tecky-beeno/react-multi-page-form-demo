import {
  IonButton,
  IonCheckbox,
  IonContent,
  IonHeader,
  IonImg,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonPage,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import React, { useState } from 'react'
import { useStorageState } from '../hooks/useStorageState'
import { EmptyObject } from '../constants'
import { useDispatch, useSelector } from 'react-redux'
import { RootState, setFileAction } from '../redux/store'
import {} from '@beenotung/tslib/file'
import {
  canvasToBlob,
  base64ToCanvas,
  dataURItoMimeType,
} from '@beenotung/tslib/image'

const Home: React.FC = () => {
  const [price, setPrice] = useStorageState('selected_price', 0)

  const [selectedRepairTypeId, setRepairTypeId] = useStorageState<
    number | null
  >('selectedRepairTypeId', null)

  const [selectedServiceTypeIdList, setSelectedServiceTypeIdList] =
    useStorageState<{ [service_id: number]: boolean }>(
      'selectedServiceTypeIdList',
      EmptyObject,
    )

  const dispatch = useDispatch()

  // const [file, setFile] = useState<null | File>(null)
  const file = useSelector((state: RootState) => state.file)

  const [preview1, setPreview1] = useStorageState('preview_1', '')

  async function submit() {
    const data = {
      selectedRepairTypeId,
      selectedServiceTypeIdList: Object.entries(selectedServiceTypeIdList)
        .filter(([key, value]) => value)
        .map(([key, value]) => +key),
    }
    console.log(data)

    const body = new FormData()
    // if (file) {
    //   body.set('file', file)
    // }

    if (preview1) {
      let canvas = await base64ToCanvas(preview1)
      let blob = await canvasToBlob(canvas)
      let file = new File([blob], 'image1.jpg', {
        lastModified: Date.now(),
        type: dataURItoMimeType(preview1),
      })
      body.set('file', file)
    }

    body.set('body', JSON.stringify(data))
    fetch('/signup', {
      method: 'POST',
      body,
    })
  }
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Page 2</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <p>Price: ${price}</p>
        <IonItem routerLink="/1" routerDirection="back">
          Page 1
        </IonItem>
        <IonLabel>File:</IonLabel>
        {file && file.name}
        {preview1 && <IonImg style={{ width: '100%' }} src={preview1} />}
        <IonButton onClick={submit}>Submit</IonButton>
        <IonItem routerLink="/3">Page 3</IonItem>
      </IonContent>
    </IonPage>
  )
}

export default Home
