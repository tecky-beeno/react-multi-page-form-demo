import {
  IonButton,
  IonButtons,
  IonCheckbox,
  IonContent,
  IonHeader,
  IonIcon,
  IonImg,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonPage,
  IonSelect,
  IonSelectOption,
  IonTextarea,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import React, { useCallback, useEffect, useState } from 'react'
import { useStorageState } from '../hooks/useStorageState'
import { EmptyObject } from '../constants'
import { alarmOutline, albumsOutline } from 'ionicons/icons'
import { selectImage } from '@beenotung/tslib/file'
import { setFileAction, RootState } from '../redux/store'
import { useDispatch, useSelector } from 'react-redux'
import { fileToBase64String } from '@beenotung/tslib/file'
import { compressImageToBase64, toImage } from '@beenotung/tslib/image'

type RepairType = {
  id: number
  type: string
  service_list: ServiceType[]
}
type ServiceType = {
  id: number
  type: string
  price: number
}

const Home: React.FC = () => {
  const repairList: RepairType[] = [
    {
      id: 1,
      type: 'air conditioner',
      service_list: [
        { id: 1, type: 'check', price: 400 },
        { id: 2, type: 'clean pipe', price: 600 },
        { id: 3, type: 'add snow seed', price: 800 },
      ],
    },
    {
      id: 2,
      type: 'pump oil smoke machine',
      service_list: [
        { id: 4, type: 'check', price: 400 },
        { id: 5, type: 'clean pipe', price: 500 },
        { id: 6, type: 'repair light bulb', price: 200 },
      ],
    },
  ]

  const [selectedRepairTypeId, setRepairTypeId] = useStorageState<
    number | null
  >('selectedRepairTypeId', null)

  const [selectedServiceTypeIdList, setSelectedServiceTypeIdList] =
    useStorageState<{ [service_id: number]: boolean }>(
      'selectedServiceTypeIdList',
      EmptyObject,
    )

  const [selectedOtherServiceType, setSelectedOtherServiceType] =
    useStorageState('selectedOtherServiceType', false)

  const [selectedOtherServiceTypeDesc, setSelectedOtherServiceTypeDesc] =
    useStorageState('selectedOtherServiceTypeDesc', '')

  const serviceList = repairList.find(
    repair => repair.id === selectedRepairTypeId,
  )?.service_list

  const [price, setPrice] = useStorageState<number>('selected_price', 0)

  const dispatch = useDispatch()

  // const [file, setFile] = useState<null | File>(null)
  const file = useSelector((state: RootState) => state.file)
  async function setFile(file: File) {
    dispatch(setFileAction(file))
    let image = await toImage(file)
    let url = compressImageToBase64({
      image,
      maximumLength: 30 * 1000,
      // mimeType: 'image/jpeg',
    })
    // let url = await fileToBase64String(file)
    setPreview1(url)
  }

  const [preview1, setPreview1] = useStorageState('preview_1', '')

  useEffect(() => {
    let sum = 0
    serviceList?.forEach(service => {
      if (selectedServiceTypeIdList[service.id]) {
        sum += service.price
      }
    })
    setPrice(sum)
  }, [selectedServiceTypeIdList, serviceList, setPrice])

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Page 1</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonList>
          <IonItem>
            <IonLabel>repair type</IonLabel>
            <IonSelect
              value={selectedRepairTypeId}
              onIonChange={e => {
                setRepairTypeId(e.detail.value)
                setSelectedServiceTypeIdList(EmptyObject)
              }}
            >
              {repairList.map(repair => (
                <IonSelectOption key={repair.id} value={repair.id}>
                  {repair.type}
                </IonSelectOption>
              ))}
            </IonSelect>
          </IonItem>
          {serviceList && (
            <>
              <IonListHeader>service type</IonListHeader>
              {serviceList.map(service => (
                <IonItem key={service.id}>
                  <IonLabel>
                    {service.type} (${service.price.toLocaleString()})
                  </IonLabel>
                  <IonCheckbox
                    slot="start"
                    checked={!!selectedServiceTypeIdList[service.id]}
                    onIonChange={e => {
                      setSelectedServiceTypeIdList({
                        ...selectedServiceTypeIdList,
                        [service.id]: e.detail.checked,
                      })
                    }}
                  />
                </IonItem>
              ))}
              <IonItem>
                <IonLabel>
                  Others (need to communicate with cifu, then quote the price
                  case by case)
                </IonLabel>
                <IonCheckbox
                  slot="start"
                  checked={selectedOtherServiceType}
                  onIonChange={e => {
                    setSelectedOtherServiceType(e.detail.checked)
                  }}
                />
              </IonItem>
            </>
          )}
          <IonItem hidden={!selectedOtherServiceType}>
            <IonTextarea
              value={selectedOtherServiceTypeDesc}
              onIonChange={e =>
                setSelectedOtherServiceTypeDesc(e.detail.value || '')
              }
              placeholder="explain your case for cifu references"
            />
          </IonItem>
        </IonList>
        <IonButtons>
          <IonButton
            onClick={() => {
              selectImage({ multiple: true }).then(files => {
                setFile(files[0])
              })
            }}
          >
            <IonIcon icon={albumsOutline}></IonIcon>
            Select image
          </IonButton>
        </IonButtons>
        {file && file.name}
        {preview1 ? 'yes' : 'no'}
        {preview1 && <img src={preview1} />}
        <IonItem routerLink="/2">Page 2</IonItem>
      </IonContent>
    </IonPage>
  )
}

export default Home
