import {
  IonButton,
  IonButtons,
  IonCheckbox,
  IonContent,
  IonHeader,
  IonImg,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonNote,
  IonPage,
  IonSelect,
  IonSelectOption,
  IonText,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import React, { useState } from 'react'
import { useStorageState } from '../hooks/useStorageState'
import { EmptyObject } from '../constants'
import { useDispatch, useSelector } from 'react-redux'
import { RootState, setFileAction } from '../redux/store'
import {} from '@beenotung/tslib/file'
import {
  canvasToBlob,
  base64ToCanvas,
  dataURItoMimeType,
} from '@beenotung/tslib/image'
import { useHistory } from 'react-router'

const Home: React.FC = () => {
  const [username, setUsername] = useStorageState('username', '')
  const [password, setPassword] = useStorageState('password', '')
  const [error, setError] = useState('')
  const history = useHistory()

  function next() {
    if (!username) {
      setError('missing username')
      return
    }
    if (!password) {
      setError('missing password')
      return
    }
    setError('')
    history.push('/1')
  }
  return (
    <IonPage>
      <IonContent className="ion-padding">
        <IonList>
          <IonItem>
            <IonLabel
              color={error === 'missing username' ? 'danger' : 'primary'}
            >
              username
            </IonLabel>
            <IonInput
              value={username}
              onIonChange={e => setUsername(e.detail.value || '')}
            ></IonInput>
          </IonItem>
          <IonText className="ion-padding" slot="end">
            {error}
          </IonText>
          <IonItem>
            <IonLabel
              color={error === 'missing password' ? 'danger' : 'primary'}
            >
              password
            </IonLabel>
            <IonInput
              type="password"
              value={password}
              onIonChange={e => setPassword(e.detail.value || '')}
            ></IonInput>
          </IonItem>
          <IonButton onClick={next}>next</IonButton>
          <p>
            <IonText>{error}</IonText>
          </p>
        </IonList>
      </IonContent>
    </IonPage>
  )
}
export default Home
