import { createStore } from 'redux'

export type RootState = {
  file?: File
}

export function setFileAction(file: File) {
  return { type: 'setFile' as const, file }
}

export type RootAction = ReturnType<typeof setFileAction>

let reducer = (state: RootState = {}, action: RootAction): RootState => {
  switch (action.type) {
    case 'setFile':
      return { file: action.file }
    default:
      return state
  }
}

export let store = createStore(reducer)
