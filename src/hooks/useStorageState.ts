import { useCallback, useEffect, useState } from 'react'

export function useStorageState<T>(key: string, defaultValue: T | (() => T)) {
  const loadState = useCallback(
    function loadState(): T {
      try {
        let text = localStorage.getItem(key)
        if (text === null) {
          return typeof defaultValue === 'function'
            ? (defaultValue as any)()
            : defaultValue
        }
        return JSON.parse(text)
      } catch (error) {
        console.error('failed to load state:', error)
        return typeof defaultValue === 'function'
          ? (defaultValue as any)()
          : defaultValue
      }
    },
    [key, defaultValue],
  )

  const [state, setState] = useState<T>(loadState)

  const saveState = useCallback(
    function saveState(state: T) {
      try {
        let text = JSON.stringify(state)
        localStorage.setItem(key, text)
      } catch (error) {
        console.error('failed to save state:', error)
      }
      setState(state)
    },
    [key, setState],
  )

  useEffect(() => {
    function reload() {
      let newState = loadState()
      if (JSON.stringify(newState) !== JSON.stringify(state)) {
        setState(newState)
      }
    }
    window.addEventListener('storage', reload)
    return () => {
      window.removeEventListener('storage', reload)
    }
  }, [loadState, state])

  return [state, saveState] as const
}
